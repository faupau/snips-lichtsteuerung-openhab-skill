#!/usr/bin/env python3
# -*- coding: utf-8 -*-

print("start")
import configparser
from hermes_python.hermes import Hermes
from hermes_python.ffi.utils import MqttOptions
from hermes_python.ontology import *
import requests             #for rest api acess to openhab
import io
import json
translation_table = {       #translation table to replace german umlauts
    "ä": "ae",
    "ö": "oe",
    "ü": "ue",
    "Ü": "Ue",
    "Ö": "Oe",
    "Ä": "Ae"
}
CONFIGURATION_ENCODING_FORMAT = "utf-8"
CONFIG_INI = "config.ini"

class SnipsConfigParser(configparser.SafeConfigParser):
    def to_dict(self):
        return {section : {option_name : option for option_name, option in self.items(section)} for section in self.sections()}

def read_configuration_file(configuration_file):
    try:
        with io.open(configuration_file, encoding=CONFIGURATION_ENCODING_FORMAT) as f:
            conf_parser = SnipsConfigParser()
            conf_parser.readfp(f)
            return conf_parser.to_dict()
    except (IOError, configparser.Error) as e:
        return dict()

def subscribe_intent_callback(hermes, intentMessage):
    conf = read_configuration_file(CONFIG_INI)
    action_wrapper(hermes, intentMessage, conf)


def action_wrapper(hermes, intentMessage, conf):
    print("intent recognized")
    openhab_port = conf['global'].get("openhab_server_port")    #getting openhab port from config.ini
    openhab_server = conf['global'].get("openhab_server_url")  #getting openhab server ip from config.ini
    room = intentMessage.slots.deviceLocation.first().value                 #gets deviceLocation slot as room
    room_without = room.translate(str.maketrans(translation_table))
    room_dimmer = room_without
    room_dimmer += "_light"
    value = 0
    if intentMessage.slots.action:
        print("action slot recognized")
        action_val = intentMessage.slots.action.first().value
        action_response = requests.get('http://{}:{}/rest/items/{}/state'.format(openhab_server, openhab_port, room_dimmer))
        response = requests.get('http://{}:{}/rest/items/{}'.format(openhab_server, openhab_port, room_dimmer))
        response_string = json.loads(response.text)
        item_type = response_string.get('type')
        val_res = 0
        if item_type == 'Switch':
            result_sentence = "Ich kann die Farbe nicht wechseln, da das Gerät nicht kompatibel ist."
        elif item_type == 'Dimmer':
            if (response.content.decode('UTF-8')) == 'NULL':
                val_res = 0
            else:
                val_res = float(action_response.content)
        elif item_type == 'Color':
            s = action_response.text
            val_res = float(s.split(',')[2])
        
        if intentMessage.slots.value:
            value_val = intentMessage.slots.value.first().value
            value_int = int(value_val)
            if action_val == "higher":
                print("+value, action val ist high")
                value = value_int + val_res
                print(value)
            elif action_val == "lower":
                print("+value, action val ist niedrig")
                value = val_res - value_int
                print(value)
        else:
            if action_val == "higher":
                print("-value, action val ist high")
                value = val_res + 20
                print(value)
            elif action_val == "lower":
                print("-value, action val ist lower")
                value = val_res - 20
    
    if intentMessage.slots.value and not intentMessage.slots.action:
        print("value aber nicht action")
        value_val = intentMessage.slots.value.first().value
        value = int(value_val)
    if value >= 100:
        print("wert zu hoch")
        value = 100
    elif value <= 0:
        print("wert zu niedrig")
        value = 0
    command = 0
    value_send = str(value)
    result_sentence = "error"
    response = requests.get('http://{}:{}/rest/items/{}'.format(openhab_server, openhab_port, room_dimmer))
    response_string = json.loads(response.text)
    item_type = response_string.get('type')
    if item_type == 'Switch':
        result_sentence = "Ich kann das Gerät nicht dimmen, da es nicht kompatibel ist."
    elif item_type == 'Dimmer':
        command = value_send
        response = requests.post('http://{}:{}/rest/items/{}'.format(openhab_server, openhab_port, room_dimmer), command)     #sending color value to the openhab rest api
        result_sentence = "Ich dimme das Licht in {} auf {} Prozent !".format(str(room), value_send)   #define result sentence
    elif item_type == 'Color':
        action_response2 = requests.get('http://{}:{}/rest/items/{}/state'.format(openhab_server, openhab_port, room_dimmer))
        st = action_response2.text
        einst = st.split(',')[0]
        zweist = st.split(',')[1]
        command = einst + "," + zweist + "," + value_send
        print(command)
        response = requests.post('http://{}:{}/rest/items/{}'.format(openhab_server, openhab_port, room_dimmer), command)     #sending color value to the openhab rest api
        result_sentence = "Ich dimme das Licht in {} auf {} Prozent !".format(str(room), value_send)   #define result sentence
        
    else:
        result_sentence = "Der Raum {} ist nicht definiert. Bitte erstelle in OpenHAB ein Eitem mit dem Namen , : {}, unterstrich, leiht. ".format(str(room),str(room))
    current_session_id = intentMessage.session_id
    hermes.publish_end_session(current_session_id, result_sentence)
if __name__ == "__main__":
    conf = read_configuration_file(CONFIG_INI)
    mqtt_server = conf['global'].get("mqtt_server")
    mqtt_opts = MqttOptions()
    with Hermes("{}:1883".format(mqtt_server)) as h:
        h.subscribe_intent("Paule:LichtDimmen", subscribe_intent_callback) \
.start()
